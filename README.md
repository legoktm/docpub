# docpub

This project provides two GitLab CI templates contained in `publish.yml` which can be used to publish documentation to
`doc.wikimedia.org`.

From your `.gitlab-ci.yml`, import `publish.yml` using a [GitLab include](https://docs.gitlab.com/ee/ci/yaml/includes.html).

## Templates
### `.docpub:build-docs`

Extend this template in one of your jobs and build your docs in it.

Variables:
 * `DOCS_DIR`: Directory where your job is generating the docs. Default is `docs`

### `.docpub:publish-docs`

Extend this template in one of your jobs. It will trigger a publishing job in the
[releases Jenkins](https://releases-jenkins.wikimedia.org/job/docpub/).

If your job uses the `needs` keyword, you will need to include your doc building job in the list of needed jobs.
Otherwise, your publishing job will not have access to the artifacts from previous stages as mentioned in
[the docs](https://docs.gitlab.com/ee/ci/yaml/#needsartifacts). This will make publishing fail.

Variables:
* `PUB_LOCATION`: Path on `doc.wikimedia.org` where you want the docs to be published. Must begin with your project's
path.

  The final publishing URL modifies GitLab's top-level groups using the following mapping:
    * /repos/<project_path> -> <project_path>
    * /cloudvps-repos/<project_path> -> cloudvps/<project_path>
    * /toolforge-repos/<project_path> -> toolforge/<project_path>

  Note that the job will display in the console the full URL where your docs are being published.

  See also variable `COVERAGE`.

  Default is your project's path


* `COVERAGE`: Whether the docs are a test coverage report. PUB_LOCATION will be pre-appended with `cover`.

  Default is False.


* `PUB_TIMEOUT`: How long the publishing pipeline will wait for your project's pipeline to finish.

  In order to access the latest docs artifacts, your pipeline must complete. You should try to build and publish your
docs near the end of your pipeline to avoid timeouts, but if you require another job to run after your publishing job
your can increase the timeout.

  Default is 10 minutes.

### Example usage

Use two different stages to build the docs first and publish second. Make sure you're generating the docs in the
directory indicated by `DOCS_DIR`. You also probably want to publish only from your default branch or some other
specially designated ref. For instance:

```yaml
include:
  - project: repos/releng/docpub
    file: includes/publish.yml

stages:
  - build-docs-stage
  - publish-docs-stage
    
.docs-job:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

build-job:
  stage: build-docs-stage
  extends:
    - .docs-job
    - .docpub:build-docs
  variables:
    DOCS_DIR: generated-documentation/docs
  script:
    - ./generate-documentation.sh

publish-job:
  stage: publish-docs-stage
  extends:
    - .docs-job
    - .docpub:publish-docs
  variables:
    PUB_LOCATION: 'your/desired/location'
```
