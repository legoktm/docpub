import logging
import sys


# Adapted from https://stackoverflow.com/a/56944256
class DefaultFormatter(logging.Formatter):
    gray = '\x1b[38;20m'
    yellow = '\x1b[33;20m'
    red = '\x1b[31;20m'
    reset = '\x1b[0m'
    msg_format = '%(asctime)s %(levelname)s %(message)s'

    FORMATS = {
        logging.DEBUG: gray + msg_format + reset,
        logging.INFO: gray + msg_format + reset,
        logging.WARNING: yellow + msg_format + reset,
        logging.ERROR: red + msg_format + reset,
        logging.CRITICAL: red + msg_format + reset
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(fmt=log_fmt, datefmt='%H:%M:%S')
        return formatter.format(record)


def set_up_logging(on_jenkins=False):
    if on_jenkins:
        # For some reason, the ANSI code for regular gray is parsed incorrectly by the Jenkins console (even when a job
        # is explicitly configured to use ANSI color output). As a workaround, we use a different gray in Jenkins
        DefaultFormatter.FORMATS[logging.INFO] = '\x1b[38;5;8m' + DefaultFormatter.msg_format + DefaultFormatter.reset

    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    logging.root.handlers[0].setFormatter(DefaultFormatter())
