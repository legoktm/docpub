#!/usr/bin/python3

import logging
import os
import re
import subprocess
import sys
import time
import zipfile

import jwt
import requests
from jwt import PyJWKClient, InvalidTokenError

source_dir = '/srv/app'
sys.path.insert(0, source_dir)

from lib.log import set_up_logging


def gl_api_get(path, **kwargs):
    res = requests.get(
        f"https://{gitlab_host}/api/v4/{path}",
        allow_redirects=True,
        **kwargs
    )
    res.raise_for_status()
    return res


def get_allowed_projects():
    allowed_projects_file = os.path.join(source_dir, 'allowed_projects')
    return [line.strip() for line in open(allowed_projects_file) if line.strip()]


# Top-level groups in our GitLab instance follow the convention of adding "repos" to its name. We don't want to pass
# this along to the published location, so we remove that portion. Names then are mapped as follows:
#   * /repos/<project_path> -> <project_path>
#   * /cloudvps-repos/<project_path> -> cloudvps/<project_path>
#   * /toolforge-repos/<project_path> -> toolforge/<project_path>
def normalize_doc_location(path):
    return re.sub(r'^([^/-]*)-?repos', r'\g<1>', path.removeprefix('/')).removeprefix('/')


def decode_jwt():
    jwks_client = PyJWKClient(f"https://{gitlab_host}/-/jwks")

    try:
        token = os.getenv('DOC_PRJ_JWT')
        return jwt.decode(
            token,
            jwks_client.get_signing_key_from_jwt(token).key,
            jwt.get_unverified_header(token)['alg'],
            audience='doc-publisher'
        )
    except InvalidTokenError as e:
        logging.error("Token invalid: %s", e)
        sys.exit(1)


def validate_project_is_allowed():
    project_path = token_data['project_path'].removesuffix('/')
    if project_path not in allowed_projects:
        logging.error("""Project "%s" is not allowed to publish""", project_path)
        sys.exit(1)


def validate_publish_location():
    normalized_project_path = normalize_doc_location(token_data['project_path'])
    if not normalized_pub_location.startswith(normalized_project_path):
        logging.error(
            """Publishing location "%s" is invalid for project "%s" """, normalized_pub_location, normalized_project_path
        )
        sys.exit(1)


def wait_for_upstream_pipeline():
    specified_timeout = os.getenv('PUB_TIMEOUT')

    def get_timeout():
        try:
            return int(specified_timeout) * 60
        except ValueError:
            logging.error('Invalid timeout specified')
            sys.exit(1)

    deadline = time.time() + get_timeout()

    logging.info(f"Waiting for up to {specified_timeout} minutes for the upstream pipeline to complete")

    pipeline_status = None
    while time.time() < deadline:
        try:
            pipeline_res = gl_api_get(f"projects/{project_id}/pipelines/{pipeline_id}")

            pipeline_status = pipeline_res.json()['status']
            if pipeline_status in ['success', 'failed', 'canceled', 'skipped']:
                break
            time.sleep(10)
        except Exception as e:
            logging.error("Failed to determine status of upstream pipeline: %s", e)
            sys.exit(1)
    else:
        logging.error('Upstream pipeline did not complete in the allotted time')
        sys.exit(1)

    if pipeline_status == 'success':
        logging.info('Upstream pipeline completed')
    else:
        logging.warning('Upstream pipeline did not complete successfully. Finishing now without publishing')
        sys.exit()


def get_docs():
    logging.info('Retrieving documentation from upstream pipeline')

    try:
        job_res = gl_api_get(f"projects/{project_id}/jobs/{job_id}/artifacts", stream=True)

        artifacts_zip = '/tmp/artifacts.zip'
        with open(artifacts_zip, 'wb') as artsf:
            # 10 MB chunks (base 10)
            for chunk in job_res.iter_content(chunk_size=10000000):
                artsf.write(chunk)
        job_res.close()
        with zipfile.ZipFile(artifacts_zip, 'r') as artsf:
            artsf.extractall(artifacts_dir)
    except Exception as e:
        logging.error("Failed to retrieve documentation from upstream pipeline: %s", e)
        sys.exit(1)

    if os.path.isdir(docs_dir) and len(os.listdir(docs_dir)) > 0:
        logging.info('Documentation retrieved')
    else:
        logging.error(f"""Specified docs directory "{docs_dir}" not found among the upstream artifacts or is empty""")
        sys.exit(1)


def publish_docs():
    def get_final_doc_location():
        if os.getenv('COVERAGE').lower() in ['true', 'yes', 'y']:
            return 'cover/' + normalized_pub_location
        return normalized_pub_location

    def create_docs_dir_at_destination():
        """
        Rsync empty dir to ensure the publishing location exists on the target
        """
        try:
            os.makedirs(f"/tmp/{final_doc_location}")
            # Rsync password must be provided in environment variable RSYNC_PASSWORD
            subprocess.check_call([
                'rsync', '--recursive', '--relative',
                final_doc_location, f"rsync://{rsync_user}@doc.discovery.wmnet/doc-auth/"
            ], cwd='/tmp')
        except Exception as e:
            logging.error("Failed to create directory for documentation at target destination: %s", e)
            sys.exit(1)

    def rsync_docs():
        try:
            # Note that `--archive` will preserve permissions and dates, but owner and group are assigned by the
            # rsync module on the target (i.e. `doc-uploader`).
            # Rsync password must be provided in environment variable RSYNC_PASSWORD
            subprocess.check_call([
                'rsync', '--archive', '--compress', '--delete-after',
                docs_dir + '/', f"rsync://{rsync_user}@doc.discovery.wmnet/doc-auth/{final_doc_location}/"
            ])
        except subprocess.CalledProcessError as e:
            logging.error("Failed to publish documentation: %s", e)
            sys.exit(1)

    logging.info('Publishing documentation')

    final_doc_location = get_final_doc_location()
    create_docs_dir_at_destination()
    rsync_docs()

    logging.info(f"Done!. You can find the documentation at https://doc.wikimedia.org/{final_doc_location} ")


artifacts_dir = '/tmp/arts'
allowed_projects = get_allowed_projects()
rsync_user = os.getenv('RSYNC_USER')
docs_dir = os.path.join(artifacts_dir, os.getenv('DOC_PRJ_DOCS_DIR'))
gitlab_host = os.getenv('CI_SERVER_HOST')
normalized_pub_location = normalize_doc_location(os.getenv('PUB_LOCATION'))

set_up_logging(True)

token_data = decode_jwt()
validate_project_is_allowed()
validate_publish_location()

project_id, pipeline_id, job_id = [token_data[key] for key in ['project_id', 'pipeline_id', 'job_id']]
wait_for_upstream_pipeline()
get_docs()
publish_docs()
