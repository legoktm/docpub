#!/usr/bin/python3

import functools
import logging
import os
import sys
import time

import requests

sys.path.insert(0, '/srv/app')

from lib.log import set_up_logging

session = requests.Session()
# Note the token is not an actual secret. We use the user/token to work around the problem described in
# https://phabricator.wikimedia.org/T338950
session.auth = ('jenkinsrelapi', '11006dc3b82f367a5a99c3ccd71dafe8c7')
session.request = functools.partial(session.request, allow_redirects=True)


def rest(url, method='get', **kwargs):
    r = getattr(session, method)(url, **kwargs)
    r.raise_for_status()
    return r


def trigger_publish_job():
    job_vars = {
        var: os.getenv(var) for var in [
            'CI_SERVER_HOST',
            'CI_PROJECT_PATH',
            'PUB_LOCATION',
            'COVERAGE',
            'PUB_TIMEOUT',
            'DOC_PRJ_JWT',
            'DOC_PRJ_DOCS_DIR'
        ]
    }
    try:
        return rest(
            f"{docpub_job_url}/buildWithParameters",
            'post',
            data=job_vars
        ).headers['Location']
    except Exception as e:
        logging.error("Failed to trigger doc publishing job: %s", e)
        sys.exit(1)


def wait_for_job_to_be_started():
    def get_job_queue_status():
        try:
            return rest(f"{job_queue_item}api/json").json()
        except Exception as e:
            logging.error("Could not check if doc publishing job started: %s", e)
            sys.exit(1)

    time.sleep(1)
    job_queue_status = get_job_queue_status()

    if job_queue_status['why'] is not None:
        deadline = time.time() + 300

        logging.info("Waiting for up to 5 minutes for the doc publishing job to be started")

        while time.time() < deadline:
            job_queue_status = get_job_queue_status()
            if job_queue_status['why'] is None:
                break
            time.sleep(10)
        else:
            logging.error("Job failed to start after 5 minutes. Aborting")
            sys.exit(1)

    return job_queue_status['executable']['number']


def log_job_location():
    try:
        job_status = rest(f"{docpub_job_url}/{job_id}/api/json").json()
        logging.info(
            f"Doc publishing job started. You can follow execution and see results at: {job_status['url']}console"
        )
    except Exception as e:
        logging.error("Could not get doc publishing job status: %s", e)
        sys.exit(1)


docpub_job_url = 'https://releases-jenkins.wikimedia.org/job/docpub'

set_up_logging()

job_queue_item = trigger_publish_job()
job_id = wait_for_job_to_be_started()
log_job_location()
